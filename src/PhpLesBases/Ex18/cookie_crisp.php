<?php

if ($_GET['action'] == 'set') {
    if ($_GET['name'] && $_GET['value']) {
        setcookie($_GET['name'], $_GET['value'], time() + 3600);
    }
}
if ($_GET['action'] == 'get' && setcookie($_GET['name'])) {
    if ($value = $_COOKIE[$_GET['name']]) {
        echo $value . "\n";
    }
} // il n'existe pas de fonction pour supprimer les cookies. Il faut mettre en négatif la valeur de temps
if ($_GET['action'] == 'del') {
    setcookie($_GET['name'], '', time() - 3600);
}
