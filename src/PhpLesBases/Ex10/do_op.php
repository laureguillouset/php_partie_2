<?php

// il faut exactement 3 paramètres
// le 2nd parametre est une opération
// le 1er et le 3e parametre doivent être des chiffres

// je prends la portion du tableau de parametres qui m'interesse
$mesparametres = array_slice($argv, 1);

// je cree mon message d'erreur
$errormessage = "Incorrect Parameters\n";

if ($argc !== 4) {
    echo "$errormessage";
    exit;
}

// j'enlève les espaces en debut et en fin de paramettre
$mesparametres[0] = trim($mesparametres[0]);
$mesparametres[1] = trim($mesparametres[1]);
$mesparametres[2] = trim($mesparametres[2]);

// je verif que parametre 0 et 2 sont des chiffres (ils ont changé de key avec le array_slice)
if (is_numeric($mesparametres[0]) && is_numeric($mesparametres[2])) {
    // switch pour une serie d'instruction ou on compare la meme variable avec une autre
    switch ($mesparametres[1]) { // focus sur le parametre [1]
        case '+':
            echo $mesparametres[0] + $mesparametres[2];
            break;
        case '-':
            echo $mesparametres[0] - $mesparametres[2];
            break;
        case '*':
            echo $mesparametres[0] * $mesparametres[2];
            break;
        case '/': // attention, si on divise par 0 => 0
            if ($mesparametres[2] == 0) {
                echo '0';
            } else {
                echo $mesparametres[0] / $mesparametres[2];
            }
            break;
        case '%':
            echo $mesparametres[0] % $mesparametres[2];
            break;
        default:
            echo $errormessage;
    }
    echo "\n";
} else {
    echo $errormessage;
    exit();
    // Si c'est pas des chiffres => message d'erreur. On quitte le programme.
}
