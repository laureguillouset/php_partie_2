<?php

function ft_split($phrase)
{
    $mots = preg_split("/[\s,]+/", $phrase, -1, PREG_SPLIT_NO_EMPTY);
    sort($mots);

    return $mots;
}
