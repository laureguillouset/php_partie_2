<?php

// j'enferme le paramètre 2 dans une variable.
$model_key = $argv[1];

// je traite les autres paramètres à partir du 3eme
$tab = array_slice($argv, 2);

// je les transforme en chaine pour utiliser preg_split()
$str = implode(' ', $tab);

// je récupère les paramètres complets clé:valeur
if (preg_match_all("/\w+:\w+/", $str, $matches)) { // =>tableau. Cette fonction regarde l'intégralité des paramètres et pas juste le 1er.
    $string = implode(' ', $matches[0]); // la réponse de preg_match_all est un booleen alors =>chaine. matches[0] parce qu'il y a un tableau dans le tableau (cf xdebug)

    // je récupère la valeur avant les ':'
    $cles = preg_split("/:\w*\s?/", $string, -1, PREG_SPLIT_NO_EMPTY);

    // je récupère la valeur après les ':'
    $valeurs = preg_split("/\s?\w*:/", $string, -1, PREG_SPLIT_NO_EMPTY);

    // je combine les tableaux: d'1 coté les clés puis de l'autre les valeurs.
    $tab_kv = array_combine($cles, $valeurs);

    foreach ($tab_kv as $key => $value) { // on veut récup le dernier parametre où apparait le argv [1] Foreach parcourt tout le tableau et resortira la derniere occurence.
        if ($model_key == $key) {
            echo $value . "\n";
        }
    }
}
