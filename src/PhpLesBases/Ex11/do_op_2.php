<?php

// // un seul parametre entre guillemets où y a un calcul
// // un new message d'erreur qd syntax pas bonne
// // espace multiple ou pas d espace entre operateur et chiffres

if ($argc != 2) {
    echo 'Incorrect Parameters';
    echo "\n";
    exit;
}

$my_arg = $argv[1];

// remove whitespace
$my_arg_trim = trim($my_arg);
// string split
$my_arg_split = str_split($my_arg_trim);
// get the first char
$first_c = $my_arg_split[0];
if (is_numeric($first_c)) {
    // save tab before implode
    $tab_op = $my_arg_split;
    // translate into string
    $tab_to_string = implode($my_arg_split);
    $flag = false;
    // iterate previous tab
    foreach ($tab_op as $op) {
        // [\*|\+|\-|\/|\%]
        // look for operator & save (flag => only once!!)
        if (preg_match('/\+|\*|\/|\-|\%/', $op) && $flag == false) {
            $flag = true;
            $op1 = $op;
        } elseif (preg_match('/\+|\*|\/|\-|\%/', $op) && $flag == true) {
            // two operators
            echo 'Syntax Error';
            echo "\n";
            exit;
        }
    }
    // separation string into array
    $new_tab = preg_split('/\+|\*|\/|\-|\%/', $tab_to_string);
    $n1 = $new_tab[0];
    $n2 = $new_tab[1];
    // do the ops
    if ($op1 == '+') {
        echo $n1 + $n2;
        echo "\n";
    } elseif ($op1 == '-') {
        echo $n1 - $n2;
        echo "\n";
    } elseif ($op1 == '*') {
        echo $n1 * $n2;
        echo "\n";
    } elseif ($op1 == '/') {
        if ($n2 != 0) {
            echo $n1 / $n2;
            echo "\n";
        } else {
            echo 0;
            echo "\n";
        }
    } elseif ($op1 == '%') {
        echo $n1 % $n2;
        echo "\n";
    }
} else {
    echo 'Syntax Error';
    echo "\n";
    exit;
}

/*
//autre code possible :

const INC_PAR = "Incorrect Parameters\n";
const SYN_ERR = "Syntax Error\n";

if (!isset($argv[1]) || isset($argv[2])) {
    echo INC_PAR;
    exit();
}

if (!preg_match_all("/^\s*(?<num1>\d*)\s*(?<ope>\+|\-|\*|\/|\%)\s*(?<num2>(?1))\s*$/", $argv[1], $tab, PREG_SET_ORDER)) {
    echo SYN_ERR;
// exit(); ou else
} else {
    switch ($tab[0]['ope']) {
    case '+':
        echo $tab[0]['num1'] + $tab[0]['num2'] . "\n";
        break;

    case '-':
        echo $tab[0]['num1'] - $tab[0]['num2'] . "\n";
        break;

    case '*':
        echo $tab[0]['num1'] * $tab[0]['num2'] . "\n";
        break;

    case '/':
        echo($tab[0]['num2'] == 0 ? '0' : $tab[0]['num1'] / $tab[0]['num2']) . "\n";
        break;

    case '%':
        echo $tab[0]['num1'] % $tab[0]['num2'] . "\n";
        break;

    default:
        echo SYN_ERR;
    }
}
*/
