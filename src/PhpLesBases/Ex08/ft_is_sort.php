<?php

// Je crée une fonction qui renvoie
// TRUE si le tableau est trié et
// FALSE si le tableau n'est pas trié.

// je cree un jumeau à mon tableau

function ft_is_sort($tab)
{
    // je cree un jumeau à mon tableau
    $tabjumeau = $tab;
    // je trie mon tableau
    sort($tabjumeau);
    // je pose la condition d'égalité
    if ($tab == $tabjumeau) {
        $ft_is_sort = true; // si ce sont les mêmes,c'est trié
    } else {
        $ft_is_sort = false; // si ce ne sont pas les mêmes,c'est pas trié
    }

    return $ft_is_sort; // mot clé pour appeler la fonction
}
