<?php

// format: Jour_de_la_semaine Numéro_du_jour Mois Année Heures:Minutes:Secondes
$regex = "/(lundi|mardi|mercredi|jeudi|vendredi|samedi|dimanche)\s(\d{1,2})\s(janvier|février|mars|avril|mai|juin|juillet|août|septembre|octobre|novembre|décembre)\s(\d{4})\s((([0-1][0-9])|(2[0-3])):([0-5][0-9]):([0-5][0-9]))/i";

// je traduis mes mois en chiffres avec un tableau pour utiliser la fonction checkdate
$tab_mois = ['janvier' => 1, 'février' => 2, 'mars' => 3, 'avril' => 4, 'mai' => 5, 'juin' => 6, 'juillet' => 7, 'août' => 8, 'septembre' => 9, 'octobre' => 10, 'novembre' => 11, 'décembre' => 12];

// texte à rajouter à mon argument
$utc_paris = 'heure normale d’Europe centrale';

// je crée une constante pour mon message d'erreur
const WR_FORM = "Wrong Format\n";

// il faut que mon $argv[1] existe
if (empty($argv[1])) {
    exit();
// condition: que le format de la date réponde à ma regex
} elseif (preg_match_all($regex, $argv[1], $match)) {
    // pour que les mois soient toujours écrit tout en minuscule
    $case_mois = mb_convert_case($match[3][0], MB_CASE_LOWER, 'UTF-8');
    // je parcours le tableau des mois et dès que je trouve le mois en question, je récupère la valeur (cad le chiffre qui correspond).
    foreach ($tab_mois as $key => $value) {
        if ($case_mois == $key) {
            $mois = $value;
            break;
        }
    }

    // la condition checkdate qui vérif que la date existe
    if (checkdate($mois, $match[2][0], $match[4][0]) == true) {
        // utilisation de IntlDateFormatter au lieu de Strtotime.
        // IntlDateFormater calculera le timestamp demandé.
        $date_to_tst = new IntlDateFormatter(
            'fr_FR',
            IntlDateFormatter::FULL,
            IntlDateFormatter::FULL,
        );

        echo $date_to_tst->parse($argv[1] . $utc_paris) . "\n";
    } else {// erreur si ne correspond 2pas à checkdate
        echo WR_FORM;
    }
} else { // erreur si correspond pas à Regex
    echo WR_FORM;
}
