<?php

// je verifie qu'il y a bien une varible et qu'elle est non nulle à l'index [1] de $argv
if (!isset($argv[1])) {
    exit();
}

// je recupère le parametre à l'index [1] du terminal
$abc = $argv[1];

// je transforme la string obtenue en tableau
$tableau = explode(' ', $abc);

// je prends le mot de l'index [0] et je le pousse en dernier
array_push($tableau, $tableau[0]);

// j'enlève le 1er element du tableau
array_shift($tableau);

// je transforme le tableau en string
$bca = implode(' ', $tableau);

// je supprime les whitespaces
$stringfinale = preg_replace('/\s+/', ' ', $bca);

// j'affiche le résultat
echo $stringfinale . "\n";

// print_r ($mot);
// foreach ($mot as $value) {
// echo "$value";
// }
