<?php

// recuperer les parametre avec argv
// convertir le tableau argv  en chaine de caractère (string)
// faire qu'il n y ait qu'un seul espace entre les mots
// retirer les espaces en debut et fin de chaine

/*
if ($argc !==2) {
exit;
$chaine = implode($separator = '', $argv);
$arg1 = preg_replace('/\s+/', ' ', $chaine);
//$chaine = trim($arg1);
echo $arg1 ;
}
*/
if ($argc != 2) {
    exit();
}
$nouvellephrase = $argv[1];

$supprespace = ('/\s+/');
$ajoutespace = (' ');
$phrasefinale = preg_replace($supprespace, $ajoutespace, $nouvellephrase);

echo trim($phrasefinale, ' ');

echo "\n";

/*
//à partir de la ligne 22 :

$nouvellephrase = preg_replace("/\s+/", ' ', trim($nouvellephrase));
echo "$nouvellephrase\n";
*/
