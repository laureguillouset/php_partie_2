<?php

echo 'Entrez un nombre: ';
while (1) {
    $input = rtrim(fgets(STDIN));
    if (is_numeric($input)) {
        if (($input % 2) == 0) {
            echo "Le chiffre $input est Pair\nEntrez un nombre: ";
        } else {
            echo "Le chiffre $input est Impair\nEntrez un nombre: ";
        }
    } else {
        echo "'$input' n'est pas un chiffre\nEntrez un nombre: ";
    }
}
