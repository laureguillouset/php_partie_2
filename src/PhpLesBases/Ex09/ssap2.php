<?php

// on recup tout les parametres de la ligne de commande sauf le nom du fichier
// dans l'ordre: alphabetique et insensible à la casse, numerique, puis le reste
// pour rappel: tri par défaut de php: caract speciaux, chiffres, alpha majuscule, alpha minuscule

// Je déclare 3 variables qui sont des tableaux
$tableau_chiffre = [];
$tableau_lettres = [];
$tableau_speciaux = [];

// J'eclate en chaine les paramettres du tableau notament ceux entre guillemets
//  Je conserve uniquement la portion du tableau à partir de l'index [1]
// je veille à supprimer les espaces avant et apres ma string.
$mots = trim((implode(' ', array_slice($argv, 1))));

// Je re-forme un tableau
$tableau = preg_split("/\s/", $mots);

// Je parcours les valeurs du tableau
foreach ($tableau as $value) {
    if (ctype_alpha($value)) {
        $tableau_lettres[] = $value;
    } // je mets les lettres dans un tableau
    elseif (ctype_digit($value)) {
        $tableau_chiffre[] = $value;
    } // je mets les numeros dans un tableau
    else {
        $tableau_speciaux[] = $value;
    } // je mets ce qui reste dans un tableau
}

// je trie
sort($tableau_chiffre, SORT_STRING); // sort_string pour check le 1er chiffre
natcasesort($tableau_lettres); // pour rendre insensible à la casse
sort($tableau_speciaux); // trie standard

// je fusionne mes 3 tableaux
$tableaufinal = array_merge($tableau_lettres, $tableau_chiffre, $tableau_speciaux);

// j'eclatte ce tableau finale en string
$resultatfinal = implode("\n", $tableaufinal) . "\n";
echo $resultatfinal;
