<?php

namespace App\POO\Ex04;

use App\POO\Ex00\Tyrion;
use App\Resources\Classes\Lannister\Lannister;
use App\Resources\Classes\Stark\Sansa;

class Jaime extends Lannister
{
    public const DRUNK = "Not even if I'm drunk !\n";
    public const DO = "Let's do this.\n";
    public const PLEASURE = "With pleasure, but only in a tower in Winterfell, then.\n";

    public function sleepWith($name)
    {
        if ($name instanceof Tyrion) { // si $name est une instance de la classe Tyrion
            echo self::DRUNK;
        } elseif ($name instanceof Sansa) {
            echo self::DO;
        } else {
            echo self::PLEASURE;
        }
    }
}
