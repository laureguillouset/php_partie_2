<?php

namespace App\POO\Ex03;

// abstrait=qui est défini dans le parent mais qui n'est pas manipulé ici. elle sera exécutée par l'enfant
abstract class House
{
    abstract public function getHouseName(); // si la methode est abstraite, la classe doit l'être aussi.

    abstract public function getHouseSeat();

    abstract public function getHouseMotto();

    public function introduce()
    {
        echo 'House ' . $this->getHouseName() . ' of ' . $this->getHouseSeat() . ' : "' . $this->getHouseMotto() . '"' . "\n";
    }
}
