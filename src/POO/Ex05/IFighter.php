<?php

namespace App\POO\Ex05;

interface IFighter
{
    public function fight();
}
