<?php

// namespace App\POO\Ex05;

// class NightsWatch implements IFighter

// {
//     static $tableaurecrutes = [];
//     public function recruit($name)
//     {
//      array_push(self::$tableaurecrutes, $name);
//      return self::$tableaurecrutes;
//     }

//     public function fight()
//     {
//         foreach (self::$tableaurecrutes as $fighter)
//         if (method_exists($fighter, 'fight'))
//         $fighter->fight();
//     }

// }

// AUTRE OPTION
// <?php

namespace App\POO\Ex05;

class NightsWatch implements IFighter
{
    public function fight()
    { // besoin de cette fonction executee dans le test
    }

    public function recruit($nom)
    {
        if (method_exists($nom, 'fight')) {
            echo $nom->fight(); // acceder à la fonction dans l'objet
        }
    }
}
