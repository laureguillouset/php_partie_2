<?php

// creation de la classe parent Targaryen

// je crée le lien ou la classe est stockée

namespace App\POO\Ex02;

class Targaryen
{
    public const BURN = 'burns alive';
    public const RESIST = 'emerges naked but unharmed';

    public function getBurned(): string
    {
        if (method_exists($this, 'resistsFire')) { // $this:Renvoi à une instance en cours
            return Targaryen::RESIST;
        } else {
            return Targaryen::BURN;
        }
    }
}

/*  AUTRE OPTION
<?php

namespace App\POO\Ex02;

class Targaryen
{
    public function resistsFire()
    {
        return false;
    }

    public function getBurned()
    {
        if ($this->resistsFire() == false) {
            return 'burns alive';
        } else {
            return 'emerges naked but unharmed';
        }
    }
}
*/
