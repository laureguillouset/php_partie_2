<?php

namespace App\POO\Ex06;

class UnholyFactory
{
    // créer tableau vide pour stocker les types de combattants que je veux absorber;
    public $list = [];

    // création de la fonction absorb (de fighters)
    public function absorb($fighter)
    {
        // verif que le type de fighter est enfant de la classe Fighter && que la proprieté type existe bien
        if (!is_subclass_of($fighter, 'Fighter') && !property_exists($fighter, 'type')) {
            echo "(Factory can't absorb this, it's not a fighter)\n";

            return $this;
            // rajout de return $this à la fin de chaque condition car sinon je retourne du vide et mon test veut un objet;
        }

        // parcours tableau pour vérifier qu'il n'y a pas deux fois le meme type de fighter
        foreach ($this->list as $combattant) {
            // si le type de combattant est présent dans le tableau ressortir la phrase
            if ($combattant->type == $fighter->type) {
                echo "(Factory already absorbed a fighter of type $fighter->type)\n";

                return $this;
            }
        }

        // je crée mon tableau ou je stock les fighters dans le tableau, et je le déclare.
        array_push($this->list, $fighter);
        echo "(Factory absorbed a fighter of type $fighter->type)\n";

        return $this;
    }

    public function fabricate($fighter)
    {
        foreach ($this->list as $combattant) {
            if ($combattant->type == $fighter) {
                echo "(Factory fabricates a fighter of type $fighter)\n";

                return $combattant;
            }
        }
        echo "(Factory hasn't absorbed any fighter of type $fighter)\n";

        return null;
    }
}

//  class UnholyFactory{

//     public $combattants = [];

//     function absorb($combattant)
//     {
//         if ($combattant instanceof Fighter){
//             //return $this
//             if (array_key_exists($combattant->$name,$this->fighter)){
//                 echo "(Factory already absorbed a fighter of type $name)\n";
//             } else {
//                 echo "(Factory absorbed a fighter of type $name)\n";
//             }

//         } else {
//             echo "Factory can't absorb this, it's not a fighter\n";
//         }
//         return $this;

//     //protected function fabricate(): void{

//     }
// }
