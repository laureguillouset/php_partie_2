<?php

namespace App\POO\Ex06;

// /// pourquoi abstraite? class abstraite ne pt pas etre instanciée. ce sont les classes filles qui le peuvent.

abstract class Fighter
{
    public $type;

    // on veut stocker dans $type les arguments passés
    public function __construct(string $type)
    {
        $this->type = $type;
    } // this fait réf à l'objet couramment utilisé.
    // -> pour exécuter notre méthode depuis l'objet.

    abstract public function fight();
}
