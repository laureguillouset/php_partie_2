<?php

// Précision du chemin via lequel on pourra retrouver la classe Tyrion

namespace App\POO\Ex00;

// j'indique le chemin de la classe Lannister déjà crée pour l'héritage ET que j'UTILISE
use App\Resources\Classes\Lannister\Lannister;

// je crée la classe Tyrion héritant de Lannister
class Tyrion extends Lannister
{
    // les propriétés. Ici ce sont des constantes.
    public const BIRTH_ANNOUNCEMENT = "My name is Tyrion\n";
    public const SIZE = 'Short';

    // la méthode
    protected function announceBirth(): void // protected parce qu'on respecte le format du prototype de la fonction de la classe parent
    {
        if ($this->needBirthAnnouncement) {
            echo parent::BIRTH_ANNOUNCEMENT; // parent = mot clé fait appel à la classe Lannister. Birth Annoncement c'est la constante
            echo self::BIRTH_ANNOUNCEMENT; // self= mot clé pour qqch utilisé uniquement pour Tyrion
        }
    }

    // ////////////// Ex04

    public const DRUNK = "Not even if I'm drunk !\n";
    public const DO = "Let's do this.\n";
    public const PLEASURE = "With pleasure, but only in a tower in Winterfell, then.\n";

    public function sleepWith($name)
    {
        if (is_subclass_of($name, 'App\Resources\Classes\Lannister\Lannister')) { // si $name est une sous classe de Lannister
            echo Tyrion::DRUNK;
        } else {
            echo Tyrion::DO;
        }
    }
}
