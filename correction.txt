Correction

Je mets les corrections ou optimisations en commentaire dans le fichier concerné. 

Liste des fichiers concernés :

- ex05 : pour optimisation.
- ex11 : proposition d'un code différent.

Plein de choses à prendre dans tes codes bien commentés. Notamment la belle optimisation du \n à la fin de ton switch dans l'ex10 (fallait y penser).

Je dois quand même te mettre un fail à cause de l'ex21 mais ce n'est qu'une convention. Tu gères.

FAILED
